/**
 * CRUD's interface
 */
export interface ICRUD {
    convertImageToText(request, response, ...args);
}

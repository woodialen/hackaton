"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class CRUDController {
    /**
     *
     * @param request
     * @param response
     * @param image
     */
    convertImageToText(request, response, image) {
        return __awaiter(this, void 0, void 0, function* () {
            // Call converting image to text function
            // set image param
            const social_image = 'idea.jpg';
            console.log(social_image);
            yield this.detectText(social_image);
            response.send(image);
        });
    }
    /**
     * Google vision: Find text at image
     * @param fileName
     */
    detectText(fileName) {
        return __awaiter(this, void 0, void 0, function* () {
            const vision = require('@google-cloud/vision');
            const client = new vision.ImageAnnotatorClient({
                keyFilename: 'woodyalen-8feadba34a62.json'
            });
            const [result] = yield client.textDetection(fileName);
            const detections = result.textAnnotations;
            console.log(detections);
            console.log('Text:');
            detections.forEach(text => console.log(text.description));
            return detections;
        });
    }
}
exports.CRUDController = CRUDController;
//# sourceMappingURL=CRUDController.js.map
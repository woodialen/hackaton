import { ICRUD} from './ICRUD';
import { Request, Response } from 'express';

export class CRUDController implements ICRUD {
    /**
     *
     * @param request
     * @param response
     * @param image
     */
    async convertImageToText(request: Request, response: Response, image) {
        // Call converting image to text function
        // set image param
        const social_image = 'idea.jpg';
        console.log(social_image);
        await this.detectText(social_image);
        response.send(image);
    }

    /**
     * Google vision: Find text at image
     * @param fileName
     */
    async detectText(fileName) {
        const vision = require('@google-cloud/vision');
        const client = new vision.ImageAnnotatorClient({
            keyFilename: 'woodyalen-8feadba34a62.json'
        });
        const [result] = await client.textDetection(fileName);
        const detections = result.textAnnotations;
        detections.forEach(text => console.log(text.description));
        return detections;
    }
}

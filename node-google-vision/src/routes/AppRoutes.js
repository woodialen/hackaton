"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Routes_1 = require("./Routes");
/**
 * appRoutes - array with all application's routes
 */
exports.appRoutes = [];
exports.appRoutes.push(Routes_1.Routes);
//# sourceMappingURL=AppRoutes.js.map
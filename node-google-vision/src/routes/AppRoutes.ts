import { Routes } from './Routes';

/**
 * appRoutes - array with all application's routes
 */
export let appRoutes = [];

appRoutes.push(Routes);

import { ImageToTextController } from '../controller/ImageToTextController';

/**
 * Status's routes
 */
export const Routes = [
    {
        path: '/api/imagetotext',
        method: 'post',
        action: ImageToTextController.convertImageToText
    },
];

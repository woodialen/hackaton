"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ImageToTextController_1 = require("../controller/ImageToTextController");
/**
 * Status's routes
 */
exports.Routes = [
    {
        path: '/api/imagetotext',
        method: 'post',
        action: ImageToTextController_1.ImageToTextController.convertImageToText
    },
];
//# sourceMappingURL=Routes.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const AppRoutes_1 = require("./routes/AppRoutes");
const path = require("path");
const app = express();
/*app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));*/
/*
app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(express.static(path.join(__dirname, '/dist/front')));
// register all application routes
AppRoutes_1.appRoutes.map(routes => {
    for (let route of routes) {
        app[route.method](route.path, (request, response, next) => {
            route.action(request, response)
                .then(() => next)
                .catch(err => next(err));
        });
    }
});
// Send all other request to the angular's app
app.get('*', function (req, res) {
    console.log(__dirname + '/dist/index.html');
    res.sendFile(path.join(__dirname + '/dist/front/index.html'));
});
const port = process.env.port || 8085;
app.listen(port);
//# sourceMappingURL=index.js.map
import { Request, Response } from 'express';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { appRoutes } from './routes/AppRoutes';
import * as path from 'path';


const app = express();
/*app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));*/

/*
app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
app.use(express.static(path.join(__dirname, '/dist/front')));

// register all application routes
appRoutes.map(routes => {
    for(let route of routes) {
        app[route.method](route.path, (request: Request, response: Response, next: Function) => {
            route.action(request, response)
                .then(() => next)
                .catch(err => next(err));
        });
    }
});

// Send all other request to the angular's app
app.get('*', function(req, res) {
    console.log(__dirname + '/dist/index.html');
    res.sendFile(path.join(__dirname + '/dist/front/index.html'));
});

const port = process.env.port || 8085;
app.listen(port);


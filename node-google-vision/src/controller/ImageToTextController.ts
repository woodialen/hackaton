import { Request, Response } from 'express';
import { CRUDController } from '../crud/CRUDController';

export class ImageToTextController extends CRUDController {
    /**
     * convertImageToText
     *
     * @param {} request
     * @param {} response
     * @param {} image
     * @returns {Promise<void>}
     */
    static async convertImageToText(request: Request, response: Response, image) {
        await new CRUDController().convertImageToText(request, response, image);
    }
}

import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {FormControl, FormGroup} from '@angular/forms';
import {RestService} from '../service/rest.service';

enum UserType {student = 1, vuz}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private rest: RestService) { }

  userType = UserType;
  activeUserTab = this.userType.student;
  isAuthTab = false;


  ngOnInit() {

  }

  setActiveTab(tab) {
    this.activeUserTab = tab;
  }




  // registration(value) {
  //   this.authService.doRegister(value)
  //     .then(res => {
  //       console.log(res);
  //       this.errorMessage = "";
  //       this.successMessage = "Your account has been created";
  //     }, err => {
  //       console.log(err);
  //       this.errorMessage = err.message;
  //       this.successMessage = "";
  //     })
  // }
  authOrReg(isAuth) {
    this.isAuthTab = isAuth;
  }
}

import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {RestService} from '../service/rest.service';
import {camelCaseToDashCase} from '@angular/platform-browser/src/dom/util';

@Component({
  selector: 'user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {
  myValidateString = `российскаяфедерацияотделением2оузмсроссиипаспортвыданавтоном.окр.-огрепоханты-мансийскомувгор.нижневартовске860-01501.11.200/колполразделениядатавылнисеобаонрaпитедедигрличиыйколчарнахм.1рицнаяподписьковаленкофамилияандрейимяанатольевичотчество22.09.1962"жамдатагор.обыполрожденияместорожденияновосибирскогорайонановосибирскойобласти67077383846707738384верaф`;

  constructor(private rest: RestService) { }

  file;
  form = new FormGroup({
    firstName: new FormControl('', [this.validator.bind(this)]),
    email: new FormControl(),
    pass: new FormControl(),
    lastName: new FormControl('', [this.validator.bind(this)]),
    middleName: new FormControl('', [this.validator.bind(this)]),
    birthday: new FormControl(),
    sex: new FormControl(),
    birthdayPlace: new FormControl(),
    regAddress: new FormControl(),
    liveAddress: new FormControl(),
    passportSeria: new FormControl('', [this.validator.bind(this)]),
    passportNumber: new FormControl('', [this.validator.bind(this)]),
    passportWhoGive: new FormControl(),
    passportDate: new FormControl(),
    passportOfficeCode: new FormControl(),
  });
  formSubmite() {
    this.rest.createUser(this.form.value).then(res => console.log(res));
  }

  ngOnInit() {
  }

  fileChanges($event): void {
    console.log($event);
    const file: File = $event.target.files[0];
    const fd = new FormData();
    fd.append('image', file, file.name);
    this.rest.sendImage(file)
      .subscribe(() => {

    });
  }


  validator(formControl: FormControl) {
    if (~this.myValidateString.indexOf(formControl.value.toLowerCase())) {
      return null;
    } else {
      return {validator: {message: 'qweqwe'}};
    }
  }
}


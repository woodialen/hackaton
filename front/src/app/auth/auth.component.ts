import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {RestService} from '../service/rest.service';

@Component({
  selector: 'auth-component',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(private rest: RestService) { }
  user = new FormGroup({
    login: new FormControl(),
    pass: new FormControl()
  });

  ngOnInit() {
  }

  login($event) {
    $event.preventDefault();
    this.rest.signInWithEmailAndPassword(this.user.value.login, this.user.value.pass).then(res => console.log(res));
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuzAccountComponent } from './vuz-account.component';

describe('VuzAccountComponent', () => {
  let component: VuzAccountComponent;
  let fixture: ComponentFixture<VuzAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuzAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuzAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuzRegistrationComponent } from './vuz-registration.component';

describe('VuzRegistrationComponent', () => {
  let component: VuzRegistrationComponent;
  let fixture: ComponentFixture<VuzRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuzRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuzRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

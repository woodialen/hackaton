import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'vuz-registration',
  templateUrl: './vuz-registration.component.html',
  styleUrls: ['./vuz-registration.component.css']
})
export class VuzRegistrationComponent implements OnInit {

  constructor() { }

  file;
  form = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    middleName: new FormControl(),
    birthday: new FormControl(),
    sex: new FormControl(),
    birthdayPlace: new FormControl(),
    regAddress: new FormControl(),
    liveAddress: new FormControl(),
    passportSeria: new FormControl(),
    passportNumber: new FormControl(),
    passportWhoGive: new FormControl(),
    passportDate: new FormControl(),
    passportOfficeCode: new FormControl(),
  });

  ngOnInit() {
  }

  fileChanges($event): void {
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.file = myReader.result;
    };
    myReader.readAsDataURL(file);
  }

}

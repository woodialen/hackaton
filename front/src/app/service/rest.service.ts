import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireStorage} from '@angular/fire/storage';
import {AngularFirestore} from '@angular/fire/firestore';
import {HttpClient} from '@angular/common/http';
import {RequestOptions} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(
    public afAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private http: HttpClient) { }


  signInWithEmailAndPassword(login, pass) {
    return this.afAuth.auth.signInWithEmailAndPassword(login, pass)
      .then(res => {
        console.log(res);
        if (res.operationType === 'signIn') {
          sessionStorage.setItem('uid', res.user.uid);
        }
      }, err => err);
  }

  createUser(user) {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.pass)
      .then( res => this.firestore.collection('user').add(user)
      );
  }

  sendImage(image) {
    const headers = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    return this.http.post('/api/imagetotext', headers)
  }
}

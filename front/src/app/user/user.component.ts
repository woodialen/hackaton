import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  routes = ['profile', 'docs', 'applications'];
  activeRoute = null;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(data => {
      if (data && data.get('tab')) {
        this.activeRoute = data.get('tab');
        console.log(this.activeRoute);
      }
    });
  }

}

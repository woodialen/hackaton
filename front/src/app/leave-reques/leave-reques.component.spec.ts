import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveRequesComponent } from './leave-reques.component';

describe('LeaveRequesComponent', () => {
  let component: LeaveRequesComponent;
  let fixture: ComponentFixture<LeaveRequesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveRequesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveRequesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
